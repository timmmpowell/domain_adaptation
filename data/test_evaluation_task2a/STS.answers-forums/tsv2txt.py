#!/usr/bin/python
# To convert the STS 2015 answer-answer tsv files into the standard STS
# represention of one tab separated sentence pair per line, please run this
# script in the directory containing the answer-answer tsv files, e.g.:
#
# In $STS_2015_HOME/answer-answer, run:
#
# $ python tsv2txt.py STS.input.answer-answer.txt

import csv
import sys

STS_2015_ANSWER_ANSWER_TSV_FILES = sorted([
"academia.stackexchange.com.tsv","history.stackexchange.com.tsv","politics.stackexchange.com.tsv","astronomy.stackexchange.com.tsv","linguistics.stackexchange.com.tsv","productivity.stackexchange.com.tsv","cogsci.stackexchange.com.tsv","outdoors.stackexchange.com.tsv","scifi.stackexchange.com.tsv","cooking.stackexchange.com.tsv","parenting.stackexchange.com.tsv","sports.stackexchange.com.tsv","english.stackexchange.com.tsv","pets.stackexchange.com.tsv","travel.stackexchange.com.tsv","fitness.stackexchange.com.tsv","philosophy.stackexchange.com.tsv","writers.stackexchange.com.tsv","gardening.stackexchange.com.tsv","photo.stackexchange.com.tsv"])

if len(sys.argv) < 2:
   print >>sys.stderr, "Usage:\n\t$ python tsv2txt.py answer-answer-pairs.txt"
   sys.exit(-1)

out_filename = sys.argv[1]

sentence_pairs = []
for tsv_file in STS_2015_ANSWER_ANSWER_TSV_FILES:
  with open(tsv_file) as fh:
    reader = csv.DictReader(fh, delimiter="\t", quotechar='|')
    for row in reader:
      sentence1 = row["sentence1"]
      sentence2 = row["sentence2"]
      sentence_pairs.append((sentence1, sentence2))

with open(out_filename, "w") as fh:
   for sentence1, sentence2 in sentence_pairs:
     print >>fh, "%s\t%s" % (sentence1, sentence2) 
