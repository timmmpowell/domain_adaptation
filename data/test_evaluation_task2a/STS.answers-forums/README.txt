Paired Answer-Answer Evaluation Data for Semantic Textual Similarity (STS) 2015
-------------------------------------------------------------------------------
Date: 2014-12-07

Description
-----------
This STS evaluation dataset consists of answer-answer pairs sampled from the
Stack Exchange Data Dump (September 26, 2014):

https://archive.org/details/stackexchange

The Stack Exchange family of websites are popular question and answer
destinations that cover a diverse array of topics. 

The answer-answer STS pairs included in this evaluation set were drawn from both
answers in response to the same question and answers in response to different
questions.

While answers provided on Stack Exchange sites can vary from a single sentence
or phrase to text spanning multiple paragraphs in length, each answer pair in
this dataset consists of a single statement between approximately 15 to 20 words
in length. For longer multi-sentence answers, we extract the single statement
from the larger text that appears to best summarize the answer using the
LexRank algorithm (Erkan and Radev 2004).

IMPORTANT: The Stack Exchange Data Dump license requires that we provide 
additional metadata that allows the recovery of the source of each answer on the
Stack Exchange site were it originated. Systems submitted to the shared task
must *NOT* make use of this meta-data in anyway to assign STS labels or to
otherwise inform the operation or training of their system.

The data is distributed in two formats:

  * STS.input.answer-answer.txt - The evaluation dataset represented with the
    common file format used for other STS datasets. This format consists of tab
    separated answer-answer pairs with each line containing a single pair.

  * Full .tsv - Tab-separated value files that includes all of the information
    necessary to comply with the Stack Exchange Data Dump license. This data
    can be viewed as is or by importing it into a spreadsheet application such
    as Google Sheets.

The included Full .tsv files are:

  * academia.stackexchange.com.tsv
  * astronomy.stackexchange.com.tsv
  * cogsci.stackexchange.com.tsv
  * cooking.stackexchange.com.tsv
  * english.stackexchange.com.tsv
  * fitness.stackexchange.com.tsv
  * gardening.stackexchange.com.tsv
  * history.stackexchange.com.tsv
  * linguistics.stackexchange.com.tsv
  * outdoors.stackexchange.com.tsv
  * parenting.stackexchange.com.tsv
  * pets.stackexchange.com.tsv
  * philosophy.stackexchange.com.tsv
  * photo.stackexchange.com.tsv
  * politics.stackexchange.com.tsv
  * productivity.stackexchange.com.tsv
  * scifi.stackexchange.com.tsv
  * sports.stackexchange.com.tsv
  * travel.stackexchange.com.tsv
  * writers.stackexchange.com.tsv

The STS.input.answer-answer.txt file can be generated from this files using the
included script tsv2txt.py, e.g.:

  $ cd $STS_2015_ANSWER_ANSWER_TSV_DIR
  $ python tsv2txt.py STS.input.answer-answer.txt

License:
--------

Stack Exchange Inc. generously made the data used to construct the STS 2015
answer-answer statement pairs available under a Creative Commons Attribution-
ShareAlike (cc-by-sa) 3.0 license. 

See the included LICENSE.txt for details.

Disclaimer
----------
The SemEval 2015 STS shared task is not affiliated with any Stack Exchange
website or Stack Exchange Inc.
